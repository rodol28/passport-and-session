const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const User = require('../models/User');

passport.serializeUser((user, done) => {
    done(null, user._id);
});

passport.deserializeUser((id, done) => {
    User.findById(id, (err, user) => {
        done(err, user);
    });
});

passport.use(new LocalStrategy(
    {usernameField: 'email'}, 
    (email, password, done) => {
        User.findOne({email}, (err, user) => {
            if(!user) {return done(null, false, {msg: 'email not found'});}
            else {
                user.comparePassword(password, (err, areEqual) => {
                    if(areEqual) {
                        return done(null, user);
                    } else {
                        return done(null, false, {msg: 'invalid password'});
                    }
                });
            } 
        })
    }
    ));

exports.isAuth = (req, res, next) => {
    if(req.isAuthenticated()) {
        return next();
    } else {
        res.status(401).send('you are not authenticated');
    }
}
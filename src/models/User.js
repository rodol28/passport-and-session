const { Schema, model } = require('mongoose');
const bcrypt = require('bcrypt-nodejs');

const userSchema = new Schema({
    email: {type: String, unique: true, required: true, lowercase: true},
    password: {type: String, required: true},
    fullname: {type: String, required: true}
}, {
    timestamps: true
});

userSchema.pre('save', function (next) {
    const user = this;
    if(!user.isModified('password')) {
        return next();
    } 

    bcrypt.genSalt(10, (err, salt) => {
        if(err) return next();

        bcrypt.hash(user.password, salt, null, (err, hash) => {
            if(err) return next();
            
            user.password = hash;
            next();
        });
    })

});


userSchema.methods.comparePassword = function(password, cb) {
    bcrypt.compare(password, this.password, (err, areEqual) => {
        if(err) return cb(err);
        
        cb(null, areEqual);
    });
}

module.exports = model('User', userSchema);
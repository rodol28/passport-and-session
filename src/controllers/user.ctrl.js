const passport = require('passport');
const User = require('../models/User');

exports.postSignUp = (req, res, next) => {
    const newUser = new User({
        email: req.body.email,
        password: req.body.password,
        fullname: req.body.fullname
    });

    User.findOne({email: req.body.email}, (err, user) => {
        if(user) {
            return res.status(400).send('user already registed');
        }
        newUser.save((err) => {
            if(err) next(err);

            req.logIn(newUser, (err) => {
                if(err) next(err);

                res.send('user created successfully');
            });
        });
    });
}

exports.postLogIn = (req, res, next) => {
    passport.authenticate('local', (err, user, info) => {
        if(err) next(err);

        if(!user) {
            return res.status(400).send('invalid email or password');
        }

        req.logIn(user, (err) => {
            if(err) next(err);

            res.send('login completed');
        })
    })(req, res, next);
}

exports.getLogOut = (req, res) => {
    req.logout();
    res.send('logout completed');
}
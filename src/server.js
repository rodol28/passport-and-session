const { config } = require('dotenv');
config();
const express = require('express');
const session = require('express-session');
const bodyParser = require('body-parser');
const passport = require('passport');
const passportConfig = require('./config/passport');
const MongoStore = require('connect-mongo')(session);
require('./database');

const app = express();

const User = require('./models/User');
const newUser = new User({
    email: 'caceresrodolfo28@gmail.com',
    password: '1234',
    fullname: 'Rodolfo Caceres'
});

newUser.save()
.then(() => {
    console.log('saved');
})
.catch((err) => {
    console.log(err);
});

app.use(session({
    secret: 'thisIsASecretKey',
    resave: true,
    saveUninitialized: true,
    store: new MongoStore({
        url: process.env.MONGO_URL,
        autoReconnect: true
    })
}));

app.use(passport.initialize());
app.use(passport.session());

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

// app.get('/', (req, res) => {
//     req.session.count = req.session.count ? req.session.count + 1 : 1; 
//     res.send(`haz visto esta pagina: ${req.session.count}`);
// });

const controlUser = require('./controllers/user.ctrl');
app.post('/signup', controlUser.postSignUp);
app.post('/signin', controlUser.postLogIn);
app.get('/logout', passportConfig.isAuth, controlUser.getLogOut);

app.get('/profile', passportConfig.isAuth, (req, res) => {
    res.json({
        user: req.user
    });
})

app.listen(3000, () => {
    console.log('server listening on port 3000');
});